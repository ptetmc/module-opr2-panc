<?php

function opr2_panc_bundle_register_ambulant_form_a() {
  $bundle = array(
    'machine_name' => 'register_ambulant_form_a',
    'fields' => array(
      array(
        'field_name' => 'field_serial_register_amb',
        'type' => 'serial',
        'cardinality' => '1',
        'instance' => 
        array (
          'label' => 'Serial Register-Amb',
          'widget' => 
          array (
            'type' => 'serial',
          ),
        ),
      ),
    ), // end fields

    'instances' => array(
      'field_history' => array(
        'label' => 'Előzmény',
      ),
      'field_abdpain' => array(
        'label' => 'Hasi fájdalom jelenleg',
      ),
      'field_outpatient_date' => array(
        'label' => 'Megjelenés dátuma',
      ),
      'field_inpatient_end' => array(
        'label' => 'Távozás dátuma',
      ),
    ),

    'groups' => array(
      'group_history' => array(
        'label' => '2. Előzmény',
      ),
      'group_anamnestic' => array(
        'label' => '3. Anamnesztikus adatok (az előző megjelenés óta)',
        'i18n' => array('label' => '3. Anamnestic data (since the last visit)'),
      ),
      'group_admission' => array(
        'label' => '5. Status',
      ),
      'group_finalreport' => array(
        'label' => '8. Ambuláns lap, zárójelentés',
        'i18n' => array('label' => '8. Ambulant sheet, final report'),
      ),
    ),

    'tree' => array(
      'field_serial_register_amb',
      'group_paging' => array(
        'group_page_1' => array(
          'group_opr_data' => array(
            'field_institute',
            'field_hospital',
            'field_opr_doctor_code',
          ),
          'group_personal' => array(
            'field_patient',
            'field_outpatient_date',
            'field_inpatient_end',
          ),
          'group_history' => array(
            'field_history',
          ),
          'group_anamnestic' => array(
            'group_alcohol' => array(
              'field_alcohol_consumption',
              'field_alcohol_freq',
              'field_alcohol_amount2',
              'field_alcohol_since2',
            ),
            'group_smoking' => array(
              'field_smoking',
              'field_smoking_amount2',
              'field_smoking_since2',
            ),
          ),
        ),
        'group_page_3' => array(
          'group_symptoms' => array(
            'group_abdpain' => array(
              'field_abdpain',
              'field_stoma_since',
              'field_abdpain_type',
              'field_abdpain_strength',
              'field_stoma_loc',
              'field_stoma_loc_detail',
            ),
            'group_nausea' => array(
              'field_nausea',
            ),
            'group_vomiting' => array(
              'field_vomiting',
              'field_vomiting_times2',
              'field_vomiting_contents2',
            ),
            'group_fever' => array(
              'field_fever',
              'field_fever_since2',
              'field_fever_amount2',
            ),
            'group_appetite' => array(
              'field_appetite',
            ),
            'group_weightloss' => array(
              'field_weightloss',
              'field_weightloss_weeks2',
              'field_weightloss_amount2',
            ),
            'group_stool' => array(
              'field_stool',
            ),
          ),
        ),
        'group_page_4' => array(
          'group_admission' => array(
            'group_admission_rows' => array(
              'group_admission_row1' => array(
                'field_bp',
                'field_bp_dias',
                'field_pulse',
              ),
              'field_abdomtender',
              'field_abdomguard',
              'group_jaundice' => array(
                'field_jaundice',
                'field_jaundice_since3',
              ),
            ),
          ),
        ),
        'group_page_5' => array(
          'group_lab_results' => array(
            'group_lab_results_inline' => array(
              'field_lab_3x_amilase',
              'field_lab_3x_lipase',
              'field_lab_bloodgasmeas',
            ),
            'group_lab_results_req' => array(
              'field_lab_result_amilase',
              'field_lab_resut_lipase',
              'field_lab_result_wbc',
              'field_lab_result_rbc',
              'field_lab_result_hg',
              'field_lab_result_htoc',
              'field_lab_result_throm',
              'field_lab_result_gluc',
              'field_lab_result_urean',
              'field_lab_result_creat',
              'field_lab_result_egfr2',
              'field_lab_result_crp2',
              'field_lab_result_asatgot',
              'field_lab_result_ldh',
              'field_lab_result_ca',
            ),
            'group_lab_results_opt' => array(
              'field_lab_result_na',
              'field_lab_result_k',
              'field_lab_result_totprot',
              'field_lab_result_alb',
              'field_lab_result_chol',
              'field_lab_result_trig',
              'field_lab_result_alatgpt',
              'field_lab_result_ggt',
              'field_lab_result_bilitot',
              'field_lab_result_dcbili',
              'field_lab_result_alph',
              'field_lab_result_esr',
              'field_lab_result_procalc2',
              'field_lab_result_iga',
              'field_lab_result_igm',
              'field_lab_result_igg',
              'field_lab_result_igg4',
              'field_lab_result_ca199',
              'field_lab_result_pao2',
              'field_lab_result_hco3',
              'field_lab_result_so2',
              'field_lab_result_swcl',
              'field_lab_result_uramil',
              'field_lab_result_urlip',
              'field_lab_result_urcreat',
            ),
          ),
        ),
        'group_page_6' => array(
          'group_imaging' => array(
            'group_imagadm_inline' => array(
              'field_imaging',
              'field_imagadm_pericardfluid',
              'field_imagadm_lunginf2',
            ),
            'group_imagadm_pancabnorm' => array(
              'field_imagadm_pancabnorm',
              'field_imagadm_pancabnorm_type',
            ),
            'group_imaging_abdus' => array(
              'field_imaging_abdus',
              'field_imaging_abdus_desc',
            ),
            'group_imaging_abdrtg' => array(
              'field_imaging_abdrtg',
              'field_imaging_abdrtg_desc',
            ),
            'group_imaging_chestrtg' => array(
              'field_imaging_chestrtg',
              'field_imaging_chestrtg_desc',
            ),
            'group_imaging_chestct' => array(
              'field_imaging_chestct',
              'field_group_imaging_chestct_desc',
            ),
            'group_imaging_abdct' => array(
              'field_imaging_abdct',
              'field_imaging_abdct_desc',
            ),
            'group_imaging_eus' => array(
              'field_imaging_eus',
              'field_imaging_eus_desc',
            ),
          ), // end imaging
        ), // page 6 end
        'group_page_8' => array(
          'group_finalreport' => array(
            'field_finalreport',
          ),
          'field_notes',
        ),
      ), // end paging
    ), // end tree
  );

  return $bundle;
}

