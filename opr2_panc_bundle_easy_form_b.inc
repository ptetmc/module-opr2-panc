<?php

function opr2_panc_bundle_easy_form_b() {
  $bundle = array(
    'machine_name' => 'easy_form_b',
    'instances' => array(
      'field_form_version' => array(
        'default_value' => array(array('value'=>'v1')),
      ),
      'field_etiology_idiopathic' => array(
        'required' => TRUE,
      ),
      'field_therapint_endosc' => array(
        'label' => '6. Intervenció, endoszkópos kezelés',
        'i18n' => array(
          'label' => '6. Interventions, endoscopic treatment',
        ),
      ),
      'field_complication_panc' => array(
        'required' => FALSE,
      ),
      'field_jaundice_since' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_jaundice2' => array(
        'required' => FALSE,
      ),
      'field_therapy_ab_med' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_therapy_ab_dose' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_therapy_ab_mode' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_field_therapy_pm_med' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_therapy_pm_dose' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_therapy_pm_type' => array(
        'settings' => array(
          'deprecated' => TRUE,
        ),
      ),
      'field_complication_death' => array(
        'label' => 'Mortalitás',
      ),
      'field_complication_death_time' => array(
        'i18n' => array(
          'label' => 'the exact time of death',
          'description' => 'e.g. 10.25 or 22.45',
        ),
      ),
    ), // end instances

    'groups' => array(
      'group_admission' => array(
        'label' => '2. A beteg státusza (ha történt vizsgálat)',
        'i18n' => array(
          'label' => '2. Status',
        ),
      ),
      'group_lab_results' => array(
        'label' => '3. Laboratórium paraméterek (ha történt vizsgálat)',
        'i18n' => array(
          'label' => '3. Lab results (if any)',
        ),
      ),
      'group_lab_results_req' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'label_visibility' => 1,
          ),
        ),
      ),
      'group_lab_results_inline' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'notes' => 'Csak artériás vérgázparamétert lehet rögzíteni. Kérjük, jelezze a vérgáz paraméterek mérési körülményeit',
            'notes_en' => 'Only arterial blood gas parameters should be registered. Please indicate the measuring condition of blood gas parameters',
          ),
        ),
      ),
      'group_lab_results_opt' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'label_visibility' => 1,
          ),
        ),
      ),
      'group_imaging' => array(
        'label' => '4. Képalkotó vizsgálatok  (ha történt vizsgálat)',
        'i18n' => array(
          'label' => '4. Imaging (if any)',
        ),
      ),
      'group_therapy' => array(
        'label' => '5. Terápia',
        'i18n' => array(
          'label' => '5. Therapy',
        ),
      ),
      'group_therapint' => array(
        'label' => '6. Intervenció, endoszkópos kezelés',
        'format_settings' => array(
          'instance_settings' => array(
            'show_label' => FALSE,
          ),
        ),
        'i18n' => array(
          'label' => '6. Interventions, endoscopic treatment',
        ),
      ),
      'group_therapint_endosc' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'singlecol_head' => '1',
            'classes' => 'group-category',
          ),
        ),
      ),
      'group_complications' => array(
        'label' => '7. Szövődmények',
        'i18n' => array(
          'label' => '7. Complications',
        ),
      ),
      'group_complication_death' => array(
        'format_settings' => array(
          'instance_settings' => array(
            'keep_descriptions' => '1',
          ),
        ),
      ),
    ), // end groups

      'tree' => array(
        'group_paging' => array(
          'group_page_1' => array(
            'field_inpatient_day',
            'field_inpatient',
            'field_form_version',
            'group_admission' => array(
              'group_admission_rows' => array(
                'group_admission_row1' => array(
                  'field_bp',
                  'field_bp_dias',
                  'field_pulse',
                ),
                'group_admission_row3' => array(
                  'field_resprate',
                  'field_bodytemp',
                ),
                'group_admission_row6' => array(
                  'field_o2sat',
                  'field_o2sat_prevtherapy',
                ),
                'group_admission_row4' => array(
                  'field_abdomtender',
                  'field_abdomguard',
                ),
                'group_admission_row5' => array(
                  'field_jaundice2',
                ),
              ),
            ),
            'group_lab_results' => array(
              'group_lab_results_req' => array(
                'field_lab_result_amilase',
                'field_lab_resut_lipase',
                'field_lab_result_wbc',
                'field_lab_result_rbc',
                'field_lab_result_hg',
                'field_lab_result_htoc',
                'field_lab_result_throm',
                'field_lab_result_gluc',
                'field_lab_result_urean',
                'field_lab_result_creat',
                'field_lab_result_egfr2',
                'field_lab_result_crp2',
                'field_lab_result_asatgot',
                'field_lab_result_ldh',
                'field_lab_result_ca',
              ),
              'group_lab_results_inline' => array(
                'field_lab_o2therapy',
              ),
              'group_lab_results_opt' => array(
                'field_lab_result_na',
                'field_lab_result_k',
                'field_lab_result_totprot',
                'field_lab_result_alb',
                'field_lab_result_chol',
                'field_lab_result_trig',
                'field_lab_result_alatgpt',
                'field_lab_result_ggt',
                'field_lab_result_bilitot',
                'field_lab_result_dcbili',
                'field_lab_result_alph',
                'field_lab_result_esr',
                'field_lab_result_procalc2',
                'field_lab_result_iga',
                'field_lab_result_igm',
                'field_lab_result_igg',
                'field_lab_result_igg4',
                'field_lab_result_ca199',
                'field_lab_result_pao2',
                'field_lab_result_hco3',
                'field_lab_result_so2',
                'field_lab_result_swcl',
                'field_lab_result_uramil',
                'field_lab_result_urlip',
                'field_lab_result_urcreat',
              ),
            ),
          ),
          'group_page_2' => array(
            'group_imaging' => array(
              'group_imaging_abdus' => array(
                'field_imaging_abdus',
                'field_imaging_abdus_desc',
              ),
              'group_imaging_abdrtg' => array(
                'field_imaging_abdrtg',
                'field_imaging_abdrtg_desc',
              ),
              'group_imaging_chestrtg' => array(
                'field_imaging_chestrtg',
                'field_imaging_chestrtg_desc',
              ),
              'group_imaging_chestct' => array(
                'field_imaging_chestct',
                'field_group_imaging_chestct_desc',
              ),
              'group_imaging_abdct' => array(
                'field_imaging_abdct',
                'field_imaging_abdct_desc',
              ),
            ),
          ),
          'group_page_3' => array(
            'group_therapy' => array(
              'group_therapy_oral' => array(
                'field_therapy_oral',
              ),
              'group_therapy_iv' => array(
                'field_therapy_iv',
              ),
              'group_therapy_iv_list' => array(
                'field_therapy_iv_type',
                'field_therapy_iv_amount',
              ),
              'group_therapy_enteral' => array(
                'field_therapy_enteral',
                'field_therapy_enteral_type',
              ),
              'group_therapy_oral_list' => array(
                'field_therapy_enteral_formula',
                'field_therapy_enteral_amount',
                'field_therapy_enteral_dil',
              ),
              'group_therapy_pm' => array(
                'field_therapy_pm',
                'field_therapy_pm_type',
                'field_therapy_pm_detail2',
              ),
              'group_therapy_pm_list' => array(
                'field_field_therapy_pm_med',
                'field_therapy_pm_dose',
              ),
              'group_therapy_ab' => array(
                'field_therapy_ab',
                'field_therapy_ab_mode',
                'field_therapy_ab_detail2',
              ),
              'group_therapy_ab_list' => array(
                'field_therapy_ab_med',
                'field_therapy_ab_dose',
              ),
              'group_therapy_ins' => array(
                'field_therapy_ins',
                'field_therapy_ins_mode',
              ),
              'group_therapy_ins_list' => array(
                'field_therapy_ins_med',
                'field_therapy_ins_dose',
              ),
              'group_therapy_intensive' => array(
                'field_therapy_intensive',
                'field_therapy_intensive_desc',
              ),
              'group_therapy_other' => array(
                'field_therapy_other',
                'field_therapy_other_desc',
              ),
            ),
          ),
          'group_page_4' => array(
            'group_therapint' => array(
              'group_therapint_endosc' => array(
                'field_therapint_endosc',
                'field_therapint_endosc_type',
                'field_therapint_endosc_stent',
              ),
              'group_therapint_ercp' => array(
                'field_therapint_ercp',
                'group_therapint_bilducan' => array(
                  'field_therapint_bilducan',
                  'field_therapint_bilducan_note',
                ),
                'group_therapint_precut' => array(
                  'field_therapint_precut',
                  'field_therapint_precut_type',
                ),
                'group_therapint_est' => array(
                  'field_therapint_est',
                  'field_therapint_est_type',
                ),
                'group_therapint_stonex' => array(
                  'field_therapint_stonex',
                ),
                'group_ercp_stent' => array(
                  'field_ercp_stent',
                  'field_ercp_stent_material',
                  'field_ercp_stent_amount',
                  'field_ercp_stent_dia',
                  'field_ercp_stent_length',
                ),
                'group_ercp_ductfill' => array(
                  'field_ercp_ductfill',
                  'field_ercp_ductfill_notes',
                ),
              ), // end ercp
              'field_therapint_ercp_desc',
            ),
            'group_complications' => array(
              'group_complication_panc' => array(
                'field_complication_panc',
                'field_complication_panc_type',
              ),
              'group_complication_orgfail' => array(
                'field_complication_orgfail',
                'field_complication_orgfail_org',
                'field_complication_orgfail_dur',
              ),
              'group_complication_death' => array(
                'field_complication_death',
                'field_complication_death_time',
              ),
            ),
            'field_notes',
            'field_admin_notes',
          ),
        ), // end paging
    ), // end tree
  );

  return $bundle;
}

function opr2_panc_bundle_easy_form_b_compatibility($version = 'v0') {
  switch ($version) {
  case 'v0':
    return array(
      'field_bp' => array(
        'required' => FALSE,
      ),
      'field_bp_dias' => array(
        'required' => FALSE,
      ),
      'field_pulse' => array(
        'required' => FALSE,
      ),
      'field_weight' => array(
        'required' => FALSE,
      ),
      'field_height' => array(
        'required' => FALSE,
      ),
      'field_resprate' => array(
        'required' => FALSE,
      ),
      'field_abdomtender' => array(
        'required' => FALSE,
      ),
      'field_abdomguard' => array(
        'required' => FALSE,
      ),
      'field_gcs' => array(
        'required' => FALSE,
      ),
      'field_therapint_endosc' => array(
        'required' => FALSE,
      ),
      'field_therapint_ercp' => array(
        'required' => FALSE,
      ),
    );
  }
}
